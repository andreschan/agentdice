const { Client } = require("discord.js");
const {token, prefix} = require('./config.json');
const WebSocket = require('ws');
const axios = require('axios');

const publicToiletId = '722355679997853767';
const diceOnlyId = '723185987903160433';
const freecoinOnlyId = '723622762119430204';

var publicToiletChannel;
var diceOnlyChannel;
var freecoinOnlyChannel;

var isSocketConnected = false;

var isPublishingToilet = true;
var isPublishingDiceOnly = true;
var isPublishingFreecoinOnly = true;

var autoRestartloop;

var scamArg = 0;

var current;

const getSeedUrl = 'https://csgoempire.com/api/v2/metadata/roulette/seeds?per_page=15&page=1';
const getAllSeedUrl = 'https://csgoempire.com/api/v2/metadata/roulette/seeds?per_page=50&page=1';
const getHistoryUrl = 'https://csgoempire.com/api/v2/metadata/roulette/history'
const client = new Client({
    disableEveryOne: true,
    prefix: "dice!"
});

var rouletteSocket = null;

var options = {
    headers: {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36'
    }
}

var cachedDiceIndex = 0;

client.on("ready", () => {
    console.log("im online now.")
    setIdleStatus();
    console.log(`Logged in as ${client.user.tag}!`);
    publicToiletChannel = client.channels.cache.get(publicToiletId);
    diceOnlyChannel = client.channels.cache.get(diceOnlyId);
    freecoinOnlyChannel = client.channels.cache.get(freecoinOnlyId);
    autoRestart();
    startWS();
    setInterval(checkConnection, 1000*5);
});

client.on("message", message => {
    if(message.content.startsWith(`${prefix}start`)) {
        if (message.channel.id == publicToiletId) {
        setWatchingStatus();

        startWS();
        } else {
            message.channel.send('係公廁到打先有用')
        }
    }
    if (message.content.startsWith(`${prefix}day`)) {
        const tempArgs = message.content.slice(prefix.length).split(' ');
        if (typeof tempArgs[1] !== 'undefined') {
            dayendReport(message.channel, tempArgs[1]);
        } else {
            dayendReport(message.channel);
        }
    }
    

    if (message.content.startsWith(`${prefix}scam`)) {
        // if (rouletteSocket != null) {
        //     rouletteSocket.ping(error => {
        //         if (error != null) {
        //             console.log(error);
        //             console.log("ping server get error")
        //             if (message.channel.id == publicToiletId) { 
        //                 publicToiletChannel.send("ping server get error");
        //             }
        //             startWS();
        //         }
        //     });
        // }
        

        if (message.channel.id == publicToiletId) {
            const tempScamArgs = message.content.slice(prefix.length).split(' ');
            const tempArg = typeof tempScamArgs[1] !== 'undefined' ? tempScamArgs[1] : 0;
            console.log(tempArg + " == " + scamArg)
            if (tempArg == scamArg && isPublishingToilet) {
                publicToiletChannel.send('SCAM撚緊啦');
            } else {
                if (isPublishingToilet) {
                    scamArg = tempArg;
                    var indicator = " ROUND ROUND射";
                    if (scamArg != 0) {
                        indicator = scamArg + " ROUND之後先射"
                    }
                    publicToiletChannel.send(indicator);
                } else {
                    scamArg = tempArg;
                    isPublishingToilet = true;
                    publicToiletChannel.send('開始SCAM');
                }
            }
        }

        // if (message.channel.id == diceOnlyId) {
        //     if (isPublishingDiceOnly) {
        //         diceOnlyChannel.send('SCAM撚緊啦');
        //     } else {
        //         isPublishingDiceOnly = true; 
        //         diceOnlyChannel.send('開始SCAM DICE');
        //     }
        // }
        
        // if (message.channel.id == freecoinOnlyId) {
        //     if (isPublishingFreecoinOnly) {
        //         freecoinOnlyChannel.send('SCAM撚緊啦');
        //     } else {
        //         isPublishingFreecoinOnly = true; 
        //         freecoinOnlyChannel.send('開始SCAM FREECOIN');
        //     }
        // }
    }

    if (message.content.startsWith(`${prefix}stop`)) {

        if (message.channel.id == publicToiletId) {
            if (isPublishingToilet) {
                isPublishingToilet = false;
                publicToiletChannel.send('唔SCAM了');
            } else {
                publicToiletChannel.send('熄撚左啦');
            }
        }

        // if (message.channel.id == diceOnlyId) {
        //     if (isPublishingDiceOnly) {
        //         isPublishingDiceOnly = false; 
        //         diceOnlyChannel.send('唔SCAM了');
        //     } else {
        //         diceOnlyChannel.send('熄撚左啦');
        //     }
        // }

        // if(message.channel.id == freecoinOnlyId) {
        //     if (isPublishingFreecoinOnly) {
        //         isPublishingFreecoinOnly = false;
        //         freecoinOnlyChannel.send('唔SCAM了');
        //     } else {
        //         freecoinOnlyChannel.send('熄撚左啦');
        //     }
        // }
    } 

    if(message.content.startsWith(`${prefix}check`)) {
        let socketMessage = isSocketConnected ? 'EMPIRE: 仲在生' : 'EMPIRE: 死左'
        let publicToiletMessage = isPublishingToilet ? '公廁: SCAM ' + scamArg : '公廁: 無SCAM'
        let countingMessage = '公廁準備數';
        if(cachedDiceIndex) {
            countingMessage = '公廁數到第 ' + cachedDiceIndex + ' 口'
        }

        message.channel.send(socketMessage + '\n' + publicToiletMessage + '\n' + countingMessage + '\n' + 'EMPIRE在生姐係DICE-ONLY 同FREECOIN都WORK緊');
    }

    if (message.content.startsWith(`${prefix}encouragement`)) {
        message.channel.send('cchei ging!');
    }
    // if (message.content.startsWith(`${prefix}rain`)) {
    //     console.log("drain command")
    //     rouletteSocket.send('40/notifications,')
    //     rouletteSocket.send("42/chat,[\"chat send\",{\"msg\":\"/lastrain\"}]")
    // }
    
})

client.login(token);

function getHistorySeed(trigger = 0) {
    axios({
        method: 'get',
        url: getSeedUrl
      }).then(function (response) {
          axios({
            method: 'get',
            url: getHistoryUrl,
            data: {
                seed: response.data.data[0].id
            }
          }).then(function (responseHistory) {        
            let previousDiceItem = responseHistory.data.rolls[0]
            let previousDiceIndex = responseHistory.data.rolls.map(roll => roll.coin == "bonus").indexOf(true)
            console.log(previousDiceItem.time);   
            let timeStrInUTC = previousDiceItem.time + " +0000";          
            var rawTime = new Date(timeStrInUTC)
            var utc = rawTime.toLocaleString('zh-TW', { timeZone: 'Asia/Taipei', hour12: false });
            var time = (utc.split(' ')[1]).slice(0, 8);
            let timeStr = "[" + time + "] "
            console.log("trigger point:" + trigger);
            let filler = "中!"
            if (previousDiceIndex == 0) {                
                if (isPublishingToilet) {
                    publicToiletChannel.send(timeStr + filler + "第 \"" + (cachedDiceIndex + 1) + "\" 口中呀! 旺一旺個群先 ");
                }                 
                if (isPublishingDiceOnly) {
                    diceOnlyChannel.send(timeStr + filler + "第 \"" + (cachedDiceIndex + 1) + "\" 口中呀! 旺一旺個群先 ");
                }
            } else if (previousDiceIndex >= trigger) {
                if (isPublishingToilet) {
                    publicToiletChannel.send(timeStr + "已經過左: " + previousDiceIndex + " round");     
                }
            }
            cachedDiceIndex = previousDiceIndex
            console.log(responseHistory.data.rolls.map(roll => roll.coin == "bonus").indexOf(true));
          });
      });
}

function stopWS() {
    if (rouletteSocket != null) {
        rouletteSocket.close();
        rouletteSocket.terminate();
        rouletteSocket = null;
    }
}

function startWS() {
    console.log("startWS")
    stopWS();

    rouletteSocket = new WebSocket("wss://roulette.csgoempire.com/s/?EIO=3&transport=websocket", "echo-protocol", options);

    rouletteSocket.addEventListener("open", function(event) {
        rouletteSocket.send("40/roulette,");
        rouletteSocket.send("40/chat,");
        rouletteSocket.send('40/notifications,');
        isSocketConnected = true;
    });

    rouletteSocket.addEventListener("message", function(event) {
        var msg = event.data;
        rouletteOnReceive(msg);
    });

    rouletteSocket.addEventListener("error", function(event) {
        isSocketConnected = false;
        publicToiletChannel.send('EMPIRE又死撚左啦 打 dstart 試多次');
    });

    rouletteSocket.addEventListener("close", function(event) {
        console.log('socket onclose');
        console.log(event.wasClean);
    });
}

function rouletteOnReceive(msg) {

    if (msg.includes("rolling")) {
        rouletteSocket.send("2");
    }

    if (msg.includes("42/roulette,[\"end\"")) {
        console.log(msg);
        console.log("args str:" + scamArg);

        if (scamArg == undefined) {
            getHistorySeed();         
        } else {
            console.log("args[1]:" + scamArg);
            var trigger = parseInt(scamArg);        
            getHistorySeed(trigger);
        }
    }

    if(msg == "42/chat,[\"site:broadcast\",{\"duration\":41,\"broadcast\":\"chatRain\"}]") {
        let time = new Date();
        var timeTime = time.toLocaleString('zh-TW', { timeZone: 'Asia/Taipei', hour12: false });            
        let timeStr = "[" + timeTime + "] "

        //var startIndex = msg.lastIndexOf("\"duration\":")
        //var endIndex = msg.indexOf(",\"broadcast")
        // et duration = msg.substring(startIndex, endIndex)
        let freeCoinMsg = timeStr + "Freecoin Freecoin Freecoin Freecoin Freecoin"
        console.log("freecoin ar diu")
        if (isPublishingFreecoinOnly) {
            freecoinOnlyChannel.send(freeCoinMsg);
        }
    } 

    // if(msg.includes("lastRainCommand")) {
    //     console.log(msg)
    // }
}

function setWatchingStatus() {
    client.user.setPresence({
        status: "online",
        activity: {
            name: "CSGOEmpire",
            type: "WATCHING"
        }
    });
}

function setIdleStatus() {
    client.user.setPresence({
        status: "idle"
    });
}

function isDice(roll) {
    return roll.coin == "bonus";
}

function autoRestart() {
    if (autoRestartloop) {
        stopAutoRestart()
    }
    autoRestartloop = setInterval(startWS, 60*1000*5);
}

function stopAutoRestart() {
    clearInterval(autoRestartloop)
}

function checkConnection() {
    if (rouletteSocket.readyState == WebSocket.CLOSED) {
        console.log(rouletteSocket.readyState);
        startWS();
    }
}

function dayendReport(channel, date) {
    axios({
        method: 'get',
        url: getAllSeedUrl
      }).then(function (response) {
        console.log("seedUrl: ")
        var seed = response.data.data[0].id;
        response.data.data.forEach(element => {
            if (element.date.includes(date)) {
                seed = element.id
                return
            } 
        });
        axios({
            method: 'get',
            url: getHistoryUrl,
            data: {
                seed: seed
            }
          }).then(function (responseHistory) {
            console.log(responseHistory.data.date)
            var cacheDice = {};
            var cacheTotal = 0;
            var cachePrevious;
            var cacheDiceSpace = [];
            for (i = 0; i < responseHistory.data.rolls.length; i++) {
                if (responseHistory.data.rolls[i].coin == 'bonus') {
                    cacheDice[i] = responseHistory.data.rolls[i]
                    if (cachePrevious == null) {
                        cachePrevious = i;
                        cacheTotal += i
                        cacheDiceSpace.push(i);
                    } else {
                        cacheDiceSpace.push(i - cachePrevious);
                        cacheTotal += i - cachePrevious;
                        cachePrevious = i
                    }
                }
            }
            let totalDice = "Total Dice: " + Object.keys(cacheDice).length + " rounds"
            let avgText = "Avg: " + cacheTotal/Object.keys(cacheDice).length
            let history = "Number of rounds for each dice: " + prettyDiceSpacing(cacheDiceSpace);
            let date = "Date: " + responseHistory.data.date
            channel.send(date + "\n\n" + avgText + "\n\n" + totalDice + "\n\n" + history);
            console.log(cacheTotal);
            console.log(cacheTotal/Object.keys(cacheDice).length);
            console.log(Object.keys(cacheDice));
            console.log(prettyDiceSpacing(cacheDiceSpace))
          });
    });

    function prettyDiceSpacing(diceSpace) {
        var cached = "";
        for(i = 0; i < diceSpace.length; i++) {
            element = diceSpace[i];
            if (i == 0) {
                cached += "\n[ " + element + ", "
            } else if (i == (diceSpace.length - 1)) {
                cached += element + " ]";
            } else if (i % 10 == 0) {
                cached += element + ", \n";
            } else  {
                cached += element + ", ";
            }
        }
        return cached;
    }
}

const express = require('express');
const app = express();

app.get('/', (req, res) => {
  res.send('Hello from App Engine!');
});

// Listen to the App Engine-specified port, or 8080 otherwise
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}...`);
});